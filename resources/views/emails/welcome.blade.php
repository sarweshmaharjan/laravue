@component('mail::message')
# Welcome New {{$name}}

You have been successfully registered through: {{$email}}.

Thanks,<br>
{{ config('app.name') }}
@endcomponent
