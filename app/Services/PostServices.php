<?php

namespace App\Services;

use App\Http\Controllers\BaseController;
use App\Http\Resources\PostResource;
use App\Models\Post\Post;
use Exception;
use Illuminate\Http\JsonResponse;

class PostServices
{
    /**
     * @var BaseController
     */
    protected $baseController;

    /**
     * PostServices constructor.
     *
     * @param BaseController $baseController
     */
    public function __construct(BaseController $baseController)
    {
        $this->baseController = $baseController;
    }

    /**
     * @return JsonResponse
     * @throws Exception
     */
    public function allWithUser(): JsonResponse
    {
        try {
            $posts = PostResource::collection(Post::with('user')->get());

            return $this->baseController->sendSuccessMessage($posts, 'success');
        } catch (Exception $e) {
            throw new Exception($e);
        }
    }
}
