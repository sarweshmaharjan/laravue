<?php

namespace App\Http\Controllers\Api\User;

use App\Events\UserHasBeenRegister;
use App\Http\Controllers\BaseController;
use App\Http\Requests\StoreUserRequest;
use App\Models\User;
use App\Services\UserServices;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class UserController extends BaseController
{
    /**
     * @var UserServices
     */
    protected $userServices;

    /**
     * UserController constructor.
     *
     * @param UserServices $userServices
     */
    public function __construct(UserServices $userServices)
    {
        $this->userServices = $userServices;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return JsonResponse|AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        try {
            return $this->userServices->all($request->all(), ['posts']);
        } catch (\Exception $e) {
            $this->setLog($e);

            return $this->sendErrorMessage($e->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreUserRequest $request
     * @return JsonResponse|AnonymousResourceCollection
     */
    public function store(StoreUserRequest $request)
    {
        try {
            $createdUser = $this->userServices->create($request->validated());
            event(new UserHasBeenRegister($createdUser));

            return $createdUser;
        } catch (\Exception $e) {
            $this->setLog($e);

            return $this->sendErrorMessage('Issue while creating user');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param $userID
     * @return JsonResponse
     */
    public function show($userID)
    {
        try {
            return $this->sendSuccessMessage($this->userServices->show($userID), 'user found');
        } catch (\Exception $e) {
            $this->setLog($e);

            return $this->sendErrorMessage('No user found');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StoreUserRequest $request
     * @param User $user
     * @return JsonResponse|AnonymousResourceCollection
     */
    public function update(StoreUserRequest $request, User $user)
    {
        try {
            return $this->userServices->edit($request->validated(), $user);
        } catch (\Exception $e) {
            $this->setLog($e);

            return $this->sendErrorMessage('No user found');
        }
    }

    /**
     * Remove specified resource.
     *
     * @param int $id
     * @return int|JsonResponse
     */
    public function destroy(int $id)
    {
        try {
            return $this->userServices->delete($id);
        } catch (\Exception $e) {
            $this->setLog($e);

            return $this->sendErrorMessage('No user found');
        }
    }
}
